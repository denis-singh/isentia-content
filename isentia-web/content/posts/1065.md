---
title: "1065"
date: 2018-10-24T11:40:31+11:00
draft: false
author: Denis
---

It appears that PL/I (and its dialects) is, or will be, the most widely
used higher level language for systems programming.
		-- J. Sammet
