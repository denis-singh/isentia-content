---
title: "535"
date: 2018-10-20T19:30:14+11:00
draft: false
author: Denis
---

Freedom is slavery.
Ignorance is strength.
War is peace.
		-- George Orwell
