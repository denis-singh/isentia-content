---
title: "138"
date: 2018-10-18T01:20:10+11:00
draft: false
author: Denis
---

I don't have any use for bodyguards, but I do have a specific use for two
highly trained certified public accountants.
		-- Elvis Presley
