---
title: "1064"
date: 2018-10-24T11:30:38+11:00
draft: false
author: Denis
---

Extreme fear can neither fight nor fly.
		-- William Shakespeare, "The Rape of Lucrece"
