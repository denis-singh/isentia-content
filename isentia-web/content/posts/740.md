---
title: "740"
date: 2018-10-22T05:40:15+11:00
draft: false
author: Denis
---

"The good Christian should beware of mathematicians and all those who make 
empty prophecies.  The danger already exists that mathematicians have made 
a covenant with the devil to darken the spirit and confine man in the 
bonds of Hell."
 -- Saint Augustine
