---
title: "435"
date: 2018-10-20T02:50:16+11:00
draft: false
author: Denis
---

Microsoft Mandatory Survey (#8)

Customers who want to upgrade to Windows 98 Second Edition must now fill
out a Microsoft survey online before they can order the bugfix/upgrade.

Question 8: If you could meet Bill Gates for one minute, what would you
	    say to him?

A. "Can you give me a loan for a million or so?" 

B. "I just love all the new features in Windows 98!" 

C. "Could you autograph this box of Windows 98 for me?" 

D. "I really enjoyed reading 'Business @ the Speed of Thought'. It's so
   cool!" 

E. "Give the government hell, Bill!" 
