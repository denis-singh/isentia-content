---
title: "478"
date: 2018-10-20T10:00:15+11:00
draft: false
author: Denis
---

You know it's going to be a long day when you get up, shave and shower,
start to get dressed and your shoes are still warm.
		-- Dean Webber
