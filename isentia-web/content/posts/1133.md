---
title: "1133"
date: 2018-10-24T23:00:35+11:00
draft: false
author: Denis
---

The people of Gideon have always believed that life is sacred.  That
the love of life is the greatest gift ... We are incapable of
destroying or interfering with the creation of that which we love so
deeply -- life in every form from fetus to developed being.
		-- Hodin of Gideon, "The Mark of Gideon", stardate 5423.4
