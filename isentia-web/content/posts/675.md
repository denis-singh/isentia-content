---
title: "675"
date: 2018-10-21T18:50:14+11:00
draft: false
author: Denis
---

Any dramatic series the producers want us to take seriously as a representation
of contemporary reality cannot be taken seriously as a representation of
anything -- except a show to be ignored by anyone capable of sitting upright
in a chair and chewing gum simultaneously.
		-- Richard Schickel
