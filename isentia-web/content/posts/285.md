---
title: "285"
date: 2018-10-19T01:50:11+11:00
draft: false
author: Denis
---

Involvement with people is always a very delicate thing --
it requires real maturity to become involved and not get all messed up.
		-- Bernard Cooke
