---
title: "1092"
date: 2018-10-24T16:10:34+11:00
draft: false
author: Denis
---

"You are old, father William," the young man said,
	"And your hair has become very white;
And yet you incessantly stand on your head --
	Do you think, at your age, it is right?"

"In my youth," father William replied to his son,
	"I feared it might injure the brain;
But, now that I'm perfectly sure I have none,
	Why, I do it again and again."

"You are old," said the youth, "as I mentioned before,
	And have grown most uncommonly fat;
Yet you turned a back-somersault in at the door --
	Pray what is the reason of that?"

"In my youth," said the sage, as he shook his grey locks,
	"I kept all my limbs very supple
By the use of this ointment -- one shilling the box --
	Allow me to sell you a couple?"
