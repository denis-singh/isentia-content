---
title: "922"
date: 2018-10-23T11:50:26+11:00
draft: false
author: Denis
---

A friend of mine won't get a divorce, because he hates lawyers more than he
hates his wife.
