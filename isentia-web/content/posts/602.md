---
title: "602"
date: 2018-10-21T06:40:16+11:00
draft: false
author: Denis
---

<Overfiend> Don't come crying to me about your "30 minute compiles"!!  I
            have to build X uphill both ways!  In the snow!  With bare
            feet! And we didn't have compilers!  We had to translate the
            C code to mnemonics OURSELVES!
<Overfiend> And I was 18 before we even had assemblers!
