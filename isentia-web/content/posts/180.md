---
title: "180"
date: 2018-10-18T08:20:12+11:00
draft: false
author: Denis
---

What if nothing exists and we're all in somebody's dream?  Or what's worse,
what if only that fat guy in the third row exists?
		-- Woody Allen, "Without Feathers"
