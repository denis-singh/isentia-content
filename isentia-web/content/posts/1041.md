---
title: "1041"
date: 2018-10-24T07:40:21+11:00
draft: false
author: Denis
---

"Nuclear war would mean abolition of most comforts, and disruption of 
normal routines, for children and adults alike."
		-- Willard F. Libby, "You *Can* Survive Atomic Attack"
