---
title: "1203"
date: 2018-10-25T10:40:33+11:00
draft: false
author: Denis
---

Carperpetuation (kar' pur pet u a shun), n.:
	The act, when vacuuming, of running over a string at least a
	dozen times, reaching over and picking it up, examining it, then
	putting it back down to give the vacuum one more chance.
		-- Rich Hall, "Sniglets"
