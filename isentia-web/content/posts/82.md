---
title: "82"
date: 2018-10-17T16:00:11+11:00
draft: false
author: Denis
---

If two people love each other, there can be no happy end to it.
		-- Ernest Hemingway
