---
title: "533"
date: 2018-10-20T19:10:15+11:00
draft: false
author: Denis
---

Americans' greatest fear is that America will turn out to have been a
phenomenon, not a civilization.
		-- Shirley Hazzard, "Transit of Venus"
