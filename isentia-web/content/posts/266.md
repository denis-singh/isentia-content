---
title: "266"
date: 2018-10-18T22:40:12+11:00
draft: false
author: Denis
---

"Every Solidarity center had piles and piles of paper .... everyone was
eating paper and a policeman was at the door.  Now all you have to do is
bend a disk."
- an anonymous member of the outlawed Polish trade union, Solidarity, 
  commenting on the benefits of using computers in support of their movement
