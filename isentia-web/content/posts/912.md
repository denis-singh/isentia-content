---
title: "912"
date: 2018-10-23T10:10:27+11:00
draft: false
author: Denis
---

A prohibitionist is the sort of man one wouldn't care to drink with
-- even if he drank.
		-- H.L. Mencken
