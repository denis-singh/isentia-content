---
title: "281"
date: 2018-10-19T01:10:12+11:00
draft: false
author: Denis
---

Sometimes a man who deserves to be looked down upon because he is a
fool is despised only because he is a lawyer.
		-- Montesquieu
