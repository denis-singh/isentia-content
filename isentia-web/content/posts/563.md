---
title: "563"
date: 2018-10-21T00:10:14+11:00
draft: false
author: Denis
---

feature, n:
	A surprising property of a program.  Occasionaly documented.  To
	call a property a feature sometimes means the author did not
	consider that case, and the program makes an unexpected, though
	not necessarily wrong response.  See BUG.  "That's not a bug, it's
	a feature!"  A bug can be changed to a feature by documenting it.
