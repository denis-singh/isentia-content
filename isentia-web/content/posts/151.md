---
title: "151"
date: 2018-10-18T03:30:12+11:00
draft: false
author: Denis
---

> If you don't need X then little VT-100 terminals are available for real 
> cheap.  Should be able to find decent ones used for around $40 each.
> For that price, they're a must for the kitchen, den, bathrooms, etc.. :)
You're right. Can you explain this to my wife? 
	-- Seen on c.o.l.development.system, on the subject of extra terminals
