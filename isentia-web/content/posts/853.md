---
title: "853"
date: 2018-10-23T00:20:21+11:00
draft: false
author: Denis
---

  William Safire's rules for writing as seen in the New York Times

     Do not put statements in the negative form.
     And don't start sentences with a conjunction.
     If you reread your work, you will find on rereading that a great
     deal of repetition can be avoided by rereading and editing.
     Never use a long word when a diminutive one will do.
     Unqualified superlatives are the worst of all.
     If any word is improper at the end of a sentence, a linking verb is.
     Avoid trendy locutions that sound flaky.
     Never, ever use repetitive redundancies.
     Also, avoid awkward or affected alliteration.
     Last, but not least, avoid cliche's like the plague.
