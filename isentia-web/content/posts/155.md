---
title: "155"
date: 2018-10-18T04:10:10+11:00
draft: false
author: Denis
---

Idiot, n.:
	A member of a large and powerful tribe whose influence in human
	affairs has always been dominant and controlling.
		-- Ambrose Bierce, "The Devil's Dictionary"
