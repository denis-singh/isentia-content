---
title: "762"
date: 2018-10-22T09:20:15+11:00
draft: false
author: Denis
---

	A novice asked the master: "I have a program that sometimes runs and
sometimes aborts.  I have followed the rules of programming, yet I am totally
baffled. What is the reason for this?"
	The master replied: "You are confused because you do not understand
the Tao.  Only a fool expects rational behavior from his fellow humans.  Why
do you expect it from a machine that humans have constructed?  Computers
simulate determinism; only the Tao is perfect.
	The rules of programming are transitory; only the Tao is eternal.
Therefore you must contemplate the Tao before you receive enlightenment."
	"But how will I know when I have received enlightenment?" asked the
novice.
	"Your program will then run correctly," replied the master.
		-- Geoffrey James, "The Tao of Programming"
