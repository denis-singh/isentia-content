---
title: "305"
date: 2018-10-19T05:10:11+11:00
draft: false
author: Denis
---

"Talk is cheap. Show me the code."

	- Linus Torvalds
