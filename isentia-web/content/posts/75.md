---
title: "75"
date: 2018-10-17T14:50:11+11:00
draft: false
author: Denis
---

"Every group has a couple of experts.  And every group has at least one idiot.
 Thus are balance and harmony (and discord) maintained.  It's sometimes hard
 to remember this in the bulk of the flamewars that all of the hassle and
 pain is generally caused by one or two highly-motivated, caustic twits."
-- Chuq Von Rospach, chuq@apple.com, about Usenet
