---
title: "114"
date: 2018-10-17T21:20:11+11:00
draft: false
author: Denis
---

"Even if you want no state, or a minimal state, then you still have to
argue it point by point.  Especially since most minimalists want to
keep exactly the economic and police system that keeps them
privileged.  That's libertarians for you -- anarchists who want police
protection from their slaves!"
		-- Coyote, in Kim Stanley Robinson's "Green Mars"
