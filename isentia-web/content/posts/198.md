---
title: "198"
date: 2018-10-18T11:20:12+11:00
draft: false
author: Denis
---

God gave man two ears and one tongue so that we listen twice as much as
we speak.
		-- Arab proverb
