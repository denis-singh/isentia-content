---
title: "745"
date: 2018-10-22T06:30:23+11:00
draft: false
author: Denis
---

Don't worry over what other people are thinking about you.  They're too
busy worrying over what you are thinking about them.
