---
title: "202"
date: 2018-10-18T12:00:13+11:00
draft: false
author: Denis
---

Mr. Rockford?  Miss Collins from the Bureau of Licenses.  We got your
renewal before the extended deadline but not your check.  I'm sorry but
at midnight you're no longer licensed as an investigator.
		-- "The Rockford Files"
