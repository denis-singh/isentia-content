---
title: "48"
date: 2018-10-16T22:22:32+11:00
draft: false
author: Denis
---

I generally avoid temptation unless I can't resist it.
		-- Mae West
