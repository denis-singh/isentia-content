---
title: "240"
date: 2018-10-18T18:20:12+11:00
draft: false
author: Denis
---

Original thought is like original sin: both happened before you were born
to people you could not have possibly met.
		-- Fran Lebowitz, "Social Studies"
