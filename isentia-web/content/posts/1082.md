---
title: "1082"
date: 2018-10-24T14:30:28+11:00
draft: false
author: Denis
---

There is an old custom among my people.  When a woman saves a man's
life, he is grateful.
		-- Nona, the Kanuto witch woman, "A Private Little War",
		   stardate 4211.8.
