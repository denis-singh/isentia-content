---
title: "365"
date: 2018-10-19T15:10:13+11:00
draft: false
author: Denis
---

Q: How many Pentium designers does it take to screw in a light bulb?
A: 1.99904274017, but that's close enough for non-technical people.
