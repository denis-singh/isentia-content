---
title: "469"
date: 2018-10-20T08:30:14+11:00
draft: false
author: Denis
---

Tux Penguin Boxing Match 

LAS VEGAS, NV -- The unofficial Linux mascot Tux the Penguin will face his arch
rival the BSD Daemon in a boxing match this Saturday night. The match is part
of the International Computer Mascot Boxing Federation's First Annual World
Championship Series. The winner will advance to face one of the Intel "Bunny
People". 

Boxing pundits favor Tux as the winner. Last week Tux won his first match in
the Championship Series against Wilbur the Gimp. "The Gimp didn't have a
chance," one spectator said. "With Tux's ability to run at top speeds of over
100mph, I don't see how he could possibly lose." The BSD Daemon, however, is
certainly a formidible opponent. While boxing rules prohibit the Daemon from
using his patented pitchfork, his pointy horns are permitted in the ring. 

Some observers think the whole Computer Mascot Boxing Federation is a fake.
"WWF is all scripted," one sports writer pointed out. "And so is this. You
actually think that a penguin is capable of boxing? The idea of a penguin
fighting a demon is patently absurd. This whole Championship Series has no
doubt been scripted. It's probably nothing more than two little kids in
penguin and demon suits duking it out in a boxing ring. What a waste of time."
