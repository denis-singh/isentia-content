---
title: "370"
date: 2018-10-19T16:00:14+11:00
draft: false
author: Denis
---

	THE LESSER-KNOWN PROGRAMMING LANGUAGES #13: SLOBOL

SLOBOL is best known for the speed, or lack of it, of its compiler.
Although many compilers allow you to take a coffee break while they
compile, SLOBOL compilers allow you to travel to Bolivia to pick the
coffee.  Forty-three programmers are known to have died of boredom
sitting at their terminals while waiting for a SLOBOL program to
compile.  Weary SLOBOL programmers often turn to a related (but
infinitely faster) language, COCAINE.
