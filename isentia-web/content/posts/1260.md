---
title: "1260"
date: 2018-10-25T20:10:38+11:00
draft: false
author: Denis
---

An expert is a person who avoids the small errors as he sweeps on to the
grand fallacy.
		-- Benjamin Stolberg
