---
title: "1263"
date: 2018-10-25T20:40:42+11:00
draft: false
author: Denis
---

"On a normal ascii line, the only safe condition to detect is a 'BREAK'
- everything else having been assigned functions by Gnu EMACS."
(By Tarl Neustaedter)
