---
title: "835"
date: 2018-10-22T21:20:23+11:00
draft: false
author: Denis
---

I have learned silence from the talkative,
toleration from the intolerant, and kindness from the unkind.
		-- Kahlil Gibran
