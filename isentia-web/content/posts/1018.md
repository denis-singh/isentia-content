---
title: "1018"
date: 2018-10-24T03:50:30+11:00
draft: false
author: Denis
---

I might be able to shoehorn a reference count in on top of the numeric
value by disallowing multiple references on scalars with a numeric value,
but it wouldn't be as clean.  I do occasionally worry about that. --lwall
