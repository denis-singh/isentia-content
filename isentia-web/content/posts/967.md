---
title: "967"
date: 2018-10-23T19:20:27+11:00
draft: false
author: Denis
---

I always choose my friends for their good looks and my enemies for their
good intellects.  Man cannot be too careful in his choice of enemies.
		-- Oscar Wilde, "The Picture of Dorian Gray"
