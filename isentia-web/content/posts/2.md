---
title: "2"
date: 2018-10-17T13:33:10+11:00
draft: false
author: Denis
---

The people of Gideon have always believed that life is sacred.  That
the love of life is the greatest gift ... We are incapable of
destroying or interfering with the creation of that which we love so
deeply -- life in every form from fetus to developed being.
		-- Hodin of Gideon, "The Mark of Gideon", stardate 5423.4
If you want to get rich from writing, write the sort of thing that's
read by persons who move their lips when the're reading to themselves.
		-- Don Marquis
How do I type "for i in *.dvi do xdvi $i done" in a GUI?
	-- Discussion in comp.os.linux.misc on the intuitiveness of interfaces
