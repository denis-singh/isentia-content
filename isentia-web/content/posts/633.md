---
title: "633"
date: 2018-10-21T11:50:16+11:00
draft: false
author: Denis
---

Fess:	Well, you must admit there is something innately humorous about
	a man chasing an invention of his own halfway across the galaxy.
Rod:	Oh yeah, it's a million yuks, sure.  But after all, isn't that the
	basic difference between robots and humans?
Fess:	What, the ability to form imaginary constructs?
Rod:	No, the ability to get hung up on them.
		-- Christopher Stasheff, "The Warlock in Spite of Himself"
