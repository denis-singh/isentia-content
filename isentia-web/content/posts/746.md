---
title: "746"
date: 2018-10-22T06:40:20+11:00
draft: false
author: Denis
---

Welcome to UNIX!  Enjoy your session!  Have a great time!  Note the
use of exclamation points!  They are a very effective method for
demonstrating excitement, and can also spice up an otherwise plain-looking
sentence!  However, there are drawbacks!  Too much unnecessary exclaiming
can lead to a reduction in the effect that an exclamation point has on
the reader!  For example, the sentence

	Jane went to the store to buy bread

should only be ended with an exclamation point if there is something
sensational about her going to the store, for example, if Jane is a
cocker spaniel or if Jane is on a diet that doesn't allow bread or if
Jane doesn't exist for some reason!  See how easy it is?!  Proper control
of exclamation points can add new meaning to your life!  Call now to receive
my free pamphlet, "The Wonder and Mystery of the Exclamation Point!"!
Enclose fifteen(!) dollars for postage and handling!  Operators are
standing by!  (Which is pretty amazing, because they're all cocker spaniels!)
