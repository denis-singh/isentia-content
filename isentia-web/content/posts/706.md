---
title: "706"
date: 2018-10-22T00:00:18+11:00
draft: false
author: Denis
---

Go not to the elves for counsel, for they will say both yes and no.
		-- J.R.R. Tolkien
