---
title: "895"
date: 2018-10-23T07:20:24+11:00
draft: false
author: Denis
---

My favorite sandwich is peanut butter, baloney, cheddar cheese, lettuce
and mayonnaise on toasted bread with catsup on the side.
		-- Senator Hubert Humphrey
