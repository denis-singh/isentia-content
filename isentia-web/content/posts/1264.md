---
title: "1264"
date: 2018-10-25T20:50:33+11:00
draft: false
author: Denis
---

Thus spake the master programmer:
	"When a program is being tested, it is too late to make design changes."
		-- Geoffrey James, "The Tao of Programming"
