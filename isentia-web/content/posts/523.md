---
title: "523"
date: 2018-10-20T17:30:13+11:00
draft: false
author: Denis
---

The default Magic Word, "Abracadabra", actually is a corruption of the
Hebrew phrase "ha-Bracha dab'ra" which means "pronounce the blessing".
