---
title: "313"
date: 2018-10-19T06:30:12+11:00
draft: false
author: Denis
---

My geometry teacher was sometimes acute, and sometimes obtuse, but always,
always, he was right.
	[That's an interesting angle.  I wonder if there are any parallels?]
