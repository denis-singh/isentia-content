---
title: "1262"
date: 2018-10-25T20:30:39+11:00
draft: false
author: Denis
---

[A computer is] like an Old Testament god, with a lot of rules and no mercy.
		-- Joseph Campbell
