---
title: "1214"
date: 2018-10-25T12:30:37+11:00
draft: false
author: Denis
---

Charlie was a chemist,
But Charlie is no more.
For what he thought was H2O,
Was H2SO4.
