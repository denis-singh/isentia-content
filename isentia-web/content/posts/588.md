---
title: "588"
date: 2018-10-21T04:20:17+11:00
draft: false
author: Denis
---

"Roman Polanski makes his own blood.  He's smart -- that's why his movies work."
-- A brilliant director at "Frank's Place"
