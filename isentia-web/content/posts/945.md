---
title: "945"
date: 2018-10-23T15:40:20+11:00
draft: false
author: Denis
---

Operating Systems Installed:
  * Debian GNU/Linux 2.1 4 CD Set ($20 from www.chguy.net; price includes
    taxes, shipping, and a $3 donation to FSF). 2 CDs are binaries, 2 CDs
    complete source code;
  * Windows 98 Second Edition Upgrade Version ($136 through Megadepot.com,
    price does not include taxes/shipping). Surprisingly, no source code
    is included.

        -- Bill Stilwell, http://linuxtoday.com/stories/8794.html
