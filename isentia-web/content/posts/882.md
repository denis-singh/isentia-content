---
title: "882"
date: 2018-10-23T05:10:23+11:00
draft: false
author: Denis
---

We all agree on the necessity of compromise.  We just can't agree on
when it's necessary to compromise.
	-- Larry Wall
