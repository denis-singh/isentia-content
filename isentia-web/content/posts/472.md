---
title: "472"
date: 2018-10-20T09:00:16+11:00
draft: false
author: Denis
---

But I always fired into the nearest hill or, failing that, into blackness.
I meant no harm;  I just liked the explosions.  And I was careful never to
kill more than I could eat.
		-- Raoul Duke
