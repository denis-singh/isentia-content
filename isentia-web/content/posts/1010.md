---
title: "1010"
date: 2018-10-24T02:30:31+11:00
draft: false
author: Denis
---

My mind can never know my body, although it has become quite friendly
with my legs.
		-- Woody Allen, on Epistemology
