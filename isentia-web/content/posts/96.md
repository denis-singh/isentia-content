---
title: "96"
date: 2018-10-17T18:20:10+11:00
draft: false
author: Denis
---

I went home with a waitress,
The way I always do.
How I was I to know?
She was with the Russians too.

I was gambling in Havana,
I took a little risk.
Send lawyers, guns, and money,
Dad, get me out of this.
		-- Warren Zevon, "Lawyers, Guns and Money"
