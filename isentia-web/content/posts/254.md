---
title: "254"
date: 2018-10-18T20:40:11+11:00
draft: false
author: Denis
---

Life is a process, not a principle, a mystery to be lived, not a problem to
be solved.
- Gerard Straub, television producer and author (stolen from Frank Herbert??)
