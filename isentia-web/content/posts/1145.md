---
title: "1145"
date: 2018-10-25T01:00:34+11:00
draft: false
author: Denis
---

IV. The time required for an object to fall twenty stories is greater than or
    equal to the time it takes for whoever knocked it off the ledge to
    spiral down twenty flights to attempt to capture it unbroken.
	Such an object is inevitably priceless, the attempt to capture it
	inevitably unsuccessful.
 V. All principles of gravity are negated by fear.
	Psychic forces are sufficient in most bodies for a shock to propel
	them directly away from the earth's surface.  A spooky noise or an
	adversary's signature sound will induce motion upward, usually to
	the cradle of a chandelier, a treetop, or the crest of a flagpole.
	The feet of a character who is running or the wheels of a speeding
	auto need never touch the ground, especially when in flight.
VI. As speed increases, objects can be in several places at once.
	This is particularly true of tooth-and-claw fights, in which a
	character's head may be glimpsed emerging from the cloud of
	altercation at several places simultaneously.  This effect is common
	as well among bodies that are spinning or being throttled.  A "wacky"
	character has the option of self-replication only at manic high
	speeds and may ricochet off walls to achieve the velocity required.
		-- Esquire, "O'Donnell's Laws of Cartoon Motion", June 1980
