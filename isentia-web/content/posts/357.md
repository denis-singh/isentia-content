---
title: "357"
date: 2018-10-19T13:50:11+11:00
draft: false
author: Denis
---

The computer should be doing the hard work.  That's what it's paid to do,
after all.
             -- Larry Wall in <199709012312.QAA08121@wall.org>
