---
title: "455"
date: 2018-10-20T06:10:12+11:00
draft: false
author: Denis
---

Sam:  What's going on, Normie?
Norm: My birthday, Sammy.  Give me a beer, stick a candle in
      it, and I'll blow out my liver.
		-- Cheers, Where Have All the Floorboards Gone

Woody: Hey, Mr. P.  How goes the search for Mr. Clavin?
Norm:  Not as well as the search for Mr. Donut.
       Found him every couple of blocks.
		-- Cheers, Head Over Hill
