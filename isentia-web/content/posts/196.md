---
title: "196"
date: 2018-10-18T11:00:12+11:00
draft: false
author: Denis
---

It destroys one's nerves to be amiable every day to the same human being.
		-- Benjamin Disraeli
