---
title: "216"
date: 2018-10-18T14:20:12+11:00
draft: false
author: Denis
---

If you can't learn to do it well, learn to enjoy doing it badly.
