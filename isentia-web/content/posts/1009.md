---
title: "1009"
date: 2018-10-24T02:20:29+11:00
draft: false
author: Denis
---

"Life begins when you can spend your spare time programming instead of
watching television."
-- Cal Keegan
