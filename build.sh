#!/bin/bash

#Update site version in dev and staging 
function updateConfig() {
        env=$1
	minor_version=`grep version config.toml  | awk '{print $3}' |  sed 's/.\(.*\)/\1/' | sed 's/\(.*\)./\1/' | awk -F '.' '{print $3}'`
	major_version=`grep version config.toml  | awk '{print $3}' |  sed 's/.\(.*\)/\1/' | sed 's/\(.*\)./\1/' | awk -F '.' '{print $2}'`
	release=`grep version config.toml  | awk '{print $3}' |  sed 's/.\(.*\)/\1/' | sed 's/\(.*\)./\1/' | awk -F '.' '{print $1}'`
        
        cp -f config.toml.template config.toml

        if [ "$env" == "dev" ]; then
		minor_version=$(($minor_version + 1))
        	new_version="${release}.${major_version}.${minor_version}"
                title="Isentia Dev Site"
                base_url="dev-isentia-web-915147944.ap-southeast-2.elb.amazonaws.com"
	else
		major_version=$(($major_version + 1))
                minor_version="0"
        	new_version="${release}.${major_version}.${minor_version}"
                title="Isentia Staging Site"
                base_url="stg-isentia-web-742618776.ap-southeast-2.elb.amazonaws.com"
	fi

        sed -i "s/<VERSION>/${new_version}/g" config.toml
        sed -i "s/<BASEURL>/${base_url}/g" config.toml
        sed -i "s/<TITLE>/${title}/g" config.toml
}

function gitCommit(){
        env=$1
        git config --global user.email "gitlab-ci@test.com"
 	git config --global user.name "gitlab-ci"
        git add --all
        git commit -am "Updated content for ${env} build ${BUILD_NUMBER}"
        git pull origin master
        git push -u origin master

	#if stg, create tag and push it
	if [ "${env}" == "staging" ]; then
        	git tag "v${BUILD_NUMBER}"
        	git push -u origin "v${BUILD_NUMBER}"
	fi
}

env=$1

#Create new posts with a fortune comment and update site version. Commit the updated code back into git
if [ "${env}" == "dev" ]; then
        git checkout master
	git pull origin master
        cd isentia-web
      
	#create new posts
	hugo new posts/${BUILD_NUMBER}.md
        fortune >> content/posts/${BUILD_NUMBER}.md
        updateConfig "${env}"
        hugo -D
        cd -
	gitCommit "${env}"

elif [ "${env}" == "staging" ]; then
        git checkout master
	git pull origin master
	cd isentia-web
        updateConfig "${env}"
	hugo -D
        cd - 
	gitCommit "${env}"

else 
	exit 1
fi

